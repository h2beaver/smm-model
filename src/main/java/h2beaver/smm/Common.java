package h2beaver.smm;

import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;

public interface Common {
    PrecisionModel SCALE_10000 = new PrecisionModel(10000);
    GeometryFactory TWD97 = new GeometryFactory(SCALE_10000, 3826);
}
