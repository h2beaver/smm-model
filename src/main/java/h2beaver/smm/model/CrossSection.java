package h2beaver.smm.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.linearref.LengthIndexedLine;

import h2beaver.smm.utils.FondusUtils;
import lombok.Builder;
import lombok.Data;
import tw.fondus.commons.json.geojson.model.crs.CoordinateReferenceSystem;
import tw.fondus.commons.json.geojson.model.feature.Feature;
import tw.fondus.commons.json.geojson.model.feature.FeatureCollection;
import tw.fondus.commons.json.geojson.model.geometry.Geometry;
import tw.fondus.commons.spatial.util.geojson.GeoJsonMapper;

@Data
@Builder
public class CrossSection {
        public enum PROPERITY_NAME {
                name, referentId, distanceFromReferent
        }

        public static class Profile {
                private List<Coordinate> coordinates = new ArrayList<>();

                public Profile() {
                }

                public Profile(Number[][] coordinates) {
                        for (int i = 0; i < coordinates.length; i++) {
                                double l = coordinates[i][0].doubleValue();
                                double z = coordinates[i][1].doubleValue();
                                this.coordinates.add(new Coordinate(l, z));
                        }
                }

                public Profile addLZ(double l, double z) {
                        coordinates.add(new Coordinate(l, z));
                        return this;
                }

                public Number[][] asPropertyValue() {
                        return coordinates.stream().map(c -> new Number[] { c.x, c.y })
                                        .toArray(size -> new Number[size][]);
                }
        }

        private String id;
        private String name;
        private Point startPoint;
        private Point endPoint;
        private Point referencePoint;
        private Profile profile;

        private Map<String, Object> properties;

        private LineString feature;

        /**
         * @return Point
         */
        public Point startPoint() {
                if (startPoint == null && feature != null) {
                        startPoint = feature.getStartPoint();
                }
                return startPoint;
        }

        /**
         * @return Point
         */
        public Point endPoint() {
                if (endPoint == null && feature != null) {
                        endPoint = feature.getEndPoint();
                }
                return startPoint;
        }

        /**
         * @return Point
         */
        public Point referencePoint() {
                return referencePoint != null ? referencePoint : startPoint();
        }

        /**
         * @return String
         */
        public String getGeoId() {
                Point p = referencePoint();
                return p == null ? id : "X" + (int) p.getX() + "-" + "Y" + (int) p.getY();
        }

        /**
         * Recalculate Profile property based on Feature geometry and referencePoint
         */
        protected void recalculateProfileProperty() {
                Profile p = new Profile();
                LengthIndexedLine lengthIndexedLine = new LengthIndexedLine(feature);
                double referenceShift = lengthIndexedLine.indexOf(referencePoint().getCoordinate());
                Stream.of(feature.getCoordinates()).forEach(c -> {
                        double l = lengthIndexedLine.indexOf(c);
                        double z = c.getZ();
                        p.addLZ(l - referenceShift, z);
                });
                this.profile = p;
                this.properties.put("profile", p.asPropertyValue());
        }

        /**
         * @return Map<String, Object>
         */
        protected Map<String, Object> getPropertiesWithId() {
                if (profile == null) {
                        recalculateProfileProperty();
                        properties.put("profile", profile.asPropertyValue());
                }
                if (!properties.containsKey("id")) {
                        properties.put("id", getGeoId());
                } else {
                        String id = (String) properties.get("id");
                        String geoId = getGeoId();
                        if (!id.equals(geoId)) {
                                properties.put("originalId", id);
                                properties.put("id", geoId);
                        }
                }
                return properties;
        }

        /**
         * @return CoordinateReferenceSystem
         */
        public CoordinateReferenceSystem getCRS() {
                return FondusUtils.fromEPSG_SRID(feature.getSRID());
        }

        /**
         * @return FeatureCollection
         */
        public FeatureCollection asGeoJsonFeatureCollection() {
                List<Feature> features = new ArrayList<>();
                features.add(asGeoJsonFeature());
                if (startPoint != null) {
                        features.add(Feature.builder().geometry(GeoJsonMapper.fromJTS(startPoint)).id("start-point")
                                        .build());
                }

                if (endPoint != null) {
                        features.add(Feature.builder().geometry(GeoJsonMapper.fromJTS(endPoint)).id("end-point")
                                        .build());
                }

                if (referencePoint != null) {
                        features.add(Feature.builder().geometry(GeoJsonMapper.fromJTS(referencePoint))
                                        .id("reference-point").build());
                }

                return FeatureCollection.builder().crs(getCRS()).features(features).build();
        }

        /**
         * @return Feature
         */
        public Feature asGeoJsonFeature() {
                return Feature.builder().geometry(GeoJsonMapper.fromJTS(feature)).id(this.getGeoId())
                                .properties(getPropertiesWithId()).build();
        }

        /**
         * @return Feature
         */
        public Feature asGeoJsonPointFeature() {
                return Feature.builder().geometry(GeoJsonMapper.fromJTS(referencePoint)).id(this.getGeoId())
                                .properties(getPropertiesWithId()).build();
        }

        /**
         * @param featureCollection
         * @return CrossSection
         */
        public static CrossSection fromGeoJsonFeatureCollection(FeatureCollection featureCollection) {
                Point startPoint = featureCollection.getFeatures().stream().filter(f -> "start-point".equals(f.getId()))
                                .findAny().map(Feature::getGeometry).map(Geometry::asPoint).map(GeoJsonMapper::toJTS)
                                .orElse(null);
                Point endPoint = featureCollection.getFeatures().stream().filter(f -> "end-point".equals(f.getId()))
                                .findAny().map(Feature::getGeometry).map(Geometry::asPoint).map(GeoJsonMapper::toJTS)
                                .orElse(null);
                Point referencePoint = featureCollection.getFeatures().stream()
                                .filter(f -> "reference-point".equals(f.getId())).findAny().map(Feature::getGeometry)
                                .map(Geometry::asPoint).map(GeoJsonMapper::toJTS).orElse(null);

                Feature feature = featureCollection.getFeatures().stream()
                                .filter(f -> Geometry.TYPE_LINE.equals(f.getGeometry().getType())).findAny()
                                .orElseGet(() -> {
                                        return Feature.builder().build();
                                });
                return CrossSection.builder().feature(GeoJsonMapper.toJTS(feature.getGeometry().asLineString()))
                                .startPoint(startPoint).endPoint(endPoint).referencePoint(referencePoint)
                                .properties(feature.getProperties()).build();
        }
}