package h2beaver.smm.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.math.Vector2D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import h2beaver.smm.model.CrossSection;
import h2beaver.smm.Common;
import tw.fondus.commons.json.geojson.model.crs.CoordinateReferenceSystem;
import tw.fondus.commons.json.geojson.model.feature.FeatureCollection;
import tw.fondus.commons.json.util.gson.GsonMapperRuntime;

public interface CrossSectionUtils {
    public static final Logger logger = LoggerFactory.getLogger("CrossSectionUtils");
    public static final CoordinateReferenceSystem GEOJSON_CRS = FondusUtils.fromEPSG_SRID(Common.TWD97.getSRID());

    public static FeatureCollection getMergedPointFeatureGeojson(Collection<CrossSection> collections) {
        return FeatureCollection.builder().crs(GEOJSON_CRS)
                .features(collections.stream().map(CrossSection::asGeoJsonPointFeature).collect(Collectors.toList()))
                .build();
    }

    public static FeatureCollection getMergedLineStringGeojson(Collection<CrossSection> collections) {
        return FeatureCollection.builder().crs(GEOJSON_CRS)
                .features(collections.stream().map(CrossSection::asGeoJsonFeature).collect(Collectors.toList()))
                .build();
    }

    public static CrossSection loadCrossSectionFromString(String json) {
        FeatureCollection featureCollection = GsonMapperRuntime.GEOJSON.toBean(json, FeatureCollection.class);
        return CrossSection.fromGeoJsonFeatureCollection(featureCollection);
    }

    public static List<CrossSection> loadCrossSectionUnderFolder(File folder, String prefix) throws IOException {
        File[] files = folder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File path, String filename) {
                // if ("test.json".equals(filename)) return true;
                if (filename.startsWith(prefix) && filename.endsWith(".json"))
                    return true;
                return false;
            }
        });
        return Stream.of(files).map(file -> {
            try {
                logger.debug("Processing " + file.getAbsolutePath());
                String json = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
                CrossSection c = loadCrossSectionFromString(json);
                logger.info("CrossSection File " + file.getAbsolutePath() + " loaded");
                return c;
            } catch (Exception e) {
                logger.warn("Fail to process CrossSection file " + file.getAbsolutePath());
            }
            return null;
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    public static CrossSection updateCrossSectionProfile(CrossSection crossSection, Coordinate startPoint,
            Coordinate endPoint, Number[][] profile) {
        int numProfilePoints = profile.length;
        double profileY0 = profile[0][0].doubleValue();

        // direction from original startPoint and endPoint
        Vector2D direction = Vector2D.create(startPoint, endPoint).normalize();
        Coordinate[] profileCoordinates = new Coordinate[numProfilePoints];
        Coordinate profileStartPoint = new Coordinate(startPoint.x, startPoint.y, profile[0][1].doubleValue());
        profileCoordinates[0] = profileStartPoint;
        for (int i = 1; i < numProfilePoints; i++) {
            Number[] yz = profile[i];
            Vector2D pointReference = direction.multiply(yz[0].doubleValue() - profileY0);
            profileCoordinates[i] = new Coordinate(profileStartPoint.x + pointReference.getX(),
                    profileStartPoint.y + pointReference.getY(), yz[1].doubleValue());
        }
        // Referance point
        Vector2D referencePointVector = direction.multiply(-profileY0);
        Coordinate referencePointCoordinate = new Coordinate(profileStartPoint.x + referencePointVector.getX(),
                profileStartPoint.y + referencePointVector.getY());
        crossSection.setReferencePoint(Common.TWD97.createPoint(referencePointCoordinate));

        LineString profileFeature = Common.TWD97.createLineString(profileCoordinates);
        crossSection.setFeature(profileFeature);
        crossSection.setStartPoint(profileFeature.getStartPoint());
        crossSection.setEndPoint(Common.TWD97.createPoint(endPoint));
        crossSection.getProperties().put("profile", profile);
        return crossSection;
    }
}
