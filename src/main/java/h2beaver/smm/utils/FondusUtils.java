package h2beaver.smm.utils;

import java.util.Collections;

import tw.fondus.commons.json.geojson.model.crs.CoordinateReferenceSystem;

public class FondusUtils {
    public static CoordinateReferenceSystem fromEPSG_SRID(int SRID) {
        return CoordinateReferenceSystem.builder()
                .properties(Collections.singletonMap("name", "urn:ogc:def:crs:EPSG::" + SRID)).build();
    }
}
